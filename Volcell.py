import numpy as np
import matplotlib.pyplot as plt
from eelab import Simulation
from eelab import Geometry
import atomdat.adas.adf15 as adf15
from eelab.dtypes import VolData
import matplotlib.cm as cm
#import aurora
from mpl_toolkits.axes_grid1.axes_divider import make_axes_locatable


def get_emission(fn, line,Te,ne, nimp, lim=10,lim2=1E19):
    '''
    Get emission from ADF15 file
    Inputs:
        fn   = name of the adf15 file
        line = idx of line (14 for 3996.13 exc, 114 for 3996.13 rec)
        Te   = array of Te values
        ne   = array of ne values
        nimp = array of impurity density values
        lim  = optional - if lim < Te value, then set emiss to 0
    '''
    mydat=adf15.ADF15()
    mydat.read(fn, v=1)
    
   # print(mydat.dat.wvl[14])
   # print(mydat.dat.wvl[64])
    mydat.print_line_index([3996,405])
    coeff=mydat.interpolate(ne,Te,line)
    
    emiss=np.multiply(coeff,np.multiply(ne,nimp))
    ind=np.where(Te > lim)
    emiss[ind]=0
    ind2=np.where(ne <lim2)
    emiss[ind2]=0

    
    return emiss





def get_coords(name):
    '''
    Gives coordinates of spectroscopic LoS for different systems
    Options:
    name  -  system name, current options:
             AEI30_OP12b
             AEF30_OP12b
    in the simulations, AEI30 is the same as AEI51 so there is no
    need to specify the 51 ports
    '''
    if name=='AEI30_OP12b':
        coords=np.array([
              [-3.5403017894,3.97650607057,-0.993995223482],
              [-3.54084972951,3.97718784752,-0.98897687935],
              [-3.54139766961,3.97786962448,-0.983958535218],
              [-3.54194560971,3.97855140143,-0.978940191085],
              [-3.54249354981,3.97923317839,-0.973921846953],
              [-3.54304148991,3.97991495534,-0.968903502821],
              [-3.54358943001,3.9805967323,-0.963885158689],
              [-3.54413737012,3.98127850926,-0.958866814557],
              [-3.54468531022,3.98196028621,-0.953848470425],
              [-3.54523325032,3.98264206317,-0.948830126293],
              [-3.54578119042,3.98332384012,-0.943811782161],
              [-3.54632913052,3.98400561708,-0.938793438029],
              [-3.54687707063,3.98468739403,-0.933775093897],
              [-3.54742501073,3.98536917099,-0.928756749765],
              [-3.54797295083,3.98605094794,-0.923738405633],
              [-3.54852089093,3.9867327249,-0.918720061501],
              [-3.54906883103,3.98741450185,-0.913701717369],
              [-3.54961677114,3.98809627881,-0.908683373237],
              [-3.55016471124,3.98877805577,-0.903665029105],
              [-3.55071265134,3.98945983272,-0.898646684973],
              [-3.55126059144,3.99014160968,-0.893628340841],
              [-3.55180853154,3.99082338663,-0.888609996709],
              [-3.55235647165,3.99150516359,-0.883591652576],
              [-3.55290441175,3.99218694054,-0.878573308444],
              [-3.55345235185,3.9928687175,-0.873554964312],
              [-3.55400029195,3.99355049445,-0.86853662018],
              [-3.55454823205,3.99423227141,-0.863518276048]
        ])
        coords*=100
        #original point for line of sight, taken from AEI51
        p0=np.array([0.90812, 2.90156, .62081])*100.
        p0r=np.sqrt(p0[0]*p0[0] + p0[1]*p0[1])
        p0[0]=p0r*np.cos(131.67*np.pi/180)
        p0[1]=p0r*np.sin(131.67*np.pi/180)
        p0[2]*=-1.

    elif name=='AEF30_OP12b':
        coords=np.array([[-3.4519    ,  4.0925    , -1.1609    ],
            [-3.44321355,  4.06895467, -1.14838175],
            [-3.43476654,  4.04549544, -1.13554213],
            [-3.42656076,  4.02212732, -1.12238386],
            [-3.41859798,  3.99885531, -1.10890976],
            [-3.4108799 ,  3.97568438, -1.09512271],
            [-3.40340815,  3.95261948, -1.08102565],
            [-3.39618435,  3.92966554, -1.0666216 ],
            [-3.38921003,  3.90682745, -1.05191363],
            [-3.38248668,  3.88411011, -1.03690488],
            [-3.37601574,  3.86151835, -1.02159857],
            [-3.36979859,  3.83905702, -1.00599796],
            [-3.36383656,  3.8167309 , -0.99010638],
            [-3.35813092,  3.79454477, -0.97392724],
            [-3.3526829 ,  3.77250336, -0.95746397],
            [-3.34749365,  3.75061139, -0.94072011],
            [-3.34256429,  3.72887353, -0.92369923],
            [-3.33789586,  3.70729442, -0.90640496],
            [-3.33348937,  3.68587869, -0.88884101],
            [-3.32934575,  3.66463089, -0.87101112],
            [-3.3254659 ,  3.64355557, -0.85291909],
            [-3.32185064,  3.62265723, -0.83456881],
            [-3.31850073,  3.60194034, -0.81596418],
            [-3.31541691,  3.58140932, -0.79710918],
            [-3.31259983,  3.56106856, -0.77800784],
            [-3.31005008,  3.54092241, -0.75866424],
            [-3.30776821,  3.52097516, -0.73908252]])
        coords*=100
        p0=np.array([-4564., 4472., 356.])/10.

    return coords, p0

dummylist1 = []
dummylist2 = []
dummylist3 = []


if __name__=='__main__':
   # s=Simulation('/u/jato/std/P5MW/N3E13D.2_SP4_CX_N')
    #s=Simulation('/u/jato/EIM-m286A-Icc500A/P5MW/N3E13D.5_FRAD.8')
    s=Simulation('/u/jato/std/P5MW/N2E13D.5_FRAD.8')
    g=Geometry('/u/jato/std/geometry')

    
    
    phi = g.phi_plane
    print(len(phi))
    ntor = g.srf_toro[0]; npol=g.srf_polo[0]; nrad=g.srf_radi[0]
    r=np.reshape(g.rg, (ntor, npol, nrad))
    z = np.reshape(g.zg, (ntor, npol, nrad))
    
    

    
    #phi=Seecool.grid.phi_plane
    #r=np.reshape(Seecool.grid.rg,(ntor,npol,nrad))
    #z=np.reshape(Seecool.grid.zg,(ntor,npol,nrad))

    #Step 1: Convert to cartesian coordinates
    x=np.zeros(np.shape(r)); y=np.zeros(np.shape(r))
    
    for i in range(len(phi)):
        x[i,:,:]=np.multiply(r[i,:,:],np.cos(phi[i]*np.pi/180))
        y[i,:,:]=np.multiply(r[i,:,:],np.sin(phi[i]*np.pi/180))
        
        my_volcel=np.zeros((ntor-1,npol-1,nrad-1))
        def tetrahedron_volume(v1,v2,v3,v4):
            matrix=np.array([
                [v1[0], v1[1], v1[2],1],
                [v2[0], v2[1], v2[2],1],
                [v3[0], v3[1], v3[2],1],
                [v4[0], v4[1], v4[2],1]])
            det=np.linalg.det(matrix)
            volume=abs(det)/6
            return volume
        
    #Step 2: Calculate cell volumes
    for i in range(ntor-1):
        print('Started toroidal index %i' %i)
            
        for j in range(npol-1):
            for k in range(nrad-1):
                x1=x[i,j,k]; x2=x[i+1,j,k]; x3=x[i+1,j+1,k]; x4=x[i,j+1,k]; x5=x[i,j,k+1]; x6=x[i+1,j,k+1]; x7=x[i+1,j+1,k+1]; x8=x[i,j+1,k+1]
                y1=y[i,j,k]; y2=y[i+1,j,k]; y3=y[i+1,j+1,k]; y4=y[i,j+1,k]; y5=y[i,j,k+1]; y6=y[i+1,j,k+1]; y7=y[i+1,j+1,k+1]; y8=y[i,j+1,k+1]
                z1=z[i,j,k]; z2=z[i+1,j,k]; z3=z[i+1,j+1,k]; z4=z[i,j+1,k]; z5=z[i,j,k+1]; z6=z[i+1,j,k+1]; z7=z[i+1,j+1,k+1]; z8=z[i,j+1,k+1]
                    
                v1=np.array([x1,y1,z1]); v2=np.array([x2,y2,z2]); v3=np.array([x3,y3,z3]); v4=np.array([x4,y4,z4])
                v5=np.array([x5,y5,z5]); v6=np.array([x6,y6,z6]); v7=np.array([x7,y7,z7]); v8=np.array([x8,y8,z8])
                    
                #tetrahedron 1: indices 1, 2, 4, and 5
                vt1=tetrahedron_volume(v1,v2,v4,v5)
                    
                #tetrahedron 2: indices 2, 3, 4 and 7
                vt2=tetrahedron_volume(v2,v3,v4,v7)
                    
                #tetrahedron 3: indices 2, 4, 5, and 7
                vt3=tetrahedron_volume(v2,v4,v5,v7)
                    
                #tetrahedron 4: indices 5, 6, 7, and 2
                vt4=tetrahedron_volume(v5,v6,v7,v2)
                    
                #tetrahedron 5: indices 4, 5, 7, and 8
                vt5=tetrahedron_volume(v4,v5,v7,v8)

                my_volcel[i,j,k]=vt1+vt2+vt3+vt4+vt5
    
                    
    # load needed parameters 
    Te=s['Te']; ni=s['ni']; niN1=s['niN1']; niN2=s['niN2']; Ti=s['Ti']; niN3=s['niN3']; niN4=s['niN4']; niN5=s['niN5']; niN6=s['niN6']; niN7=s['niN7']
                    
    idcell = ni.grid.idcell
    ind=np.where(idcell < ni.grid.nc_pl)

    ni_volume=np.zeros(np.shape(idcell))
    ni_volume[ind] = ni.values[idcell[ind]]
    ni_volume = np.reshape(ni_volume,(ntor-1, npol-1, nrad-1))
    niN1_volume=np.zeros(np.shape(idcell))
    niN1_volume[ind] = niN1.values[idcell[ind]]
    niN1_volume = np.reshape(niN1_volume, (ntor-1, npol-1,nrad-1))
    niN2_volume=np.zeros(np.shape(idcell))
    niN2_volume[ind] = niN2.values[idcell[ind]]
    niN2_volume=np.reshape(niN2_volume, (ntor-1, npol-1, nrad-1))
    niN3_volume=np.zeros(np.shape(idcell))
    niN3_volume[ind] = niN3.values[idcell[ind]]
    niN3_volume=np.reshape(niN3_volume, (ntor-1, npol-1, nrad-1))
    niN4_volume=np.zeros(np.shape(idcell))
    niN4_volume[ind] = niN4.values[idcell[ind]]
    niN4_volume = np.reshape(niN4_volume, (ntor-1, npol-1, nrad-1))
    niN5_volume=np.zeros(np.shape(idcell))
    niN5_volume[ind] = niN5.values[idcell[ind]]
    niN5_volume = np.reshape(niN5_volume, (ntor-1, npol-1, nrad-1))
    niN6_volume=np.zeros(np.shape(idcell))
    niN6_volume[ind] = niN6.values[idcell[ind]]
    niN6_volume = np.reshape(niN6_volume, (ntor-1, npol-1, nrad-1))
    niN7_volume=np.zeros(np.shape(idcell))
    niN7_volume[ind] = niN7.values[idcell[ind]]
    niN7_volume= np.reshape(niN7_volume, (ntor-1, npol-1, nrad-1))
    Te_volume = np.zeros(np.shape(idcell))
    Te_volume[ind] = Te.values[idcell[ind]]
    Te_volume = np.reshape(Te_volume, (ntor-1, npol-1, nrad-1))
    
    
    Cell_volume = np.zeros(np.shape(idcell))
    Cell_volume = np.reshape(my_volcel, (ntor-1, npol-1, nrad-1))
    
   

    
    




  #  Volume_tet1 =  np.abs( (1/6)*( (X2-X1)*((Y4-Y1)*(Z5-Z1)-(Z4-Z1)*(Y5-Y1)) + (Y2-Y1)*((Z4-Z1)*(X5-X1) - (X4-X1)*(Z5-Z1)) + (Z2-Z1)*((X4-X1)*(Y5-Y1) - (Y4-Y1)*(X5-X1))))
  #  Volume_tet2 =  np.abs( (1/6)*( (X4-X2)*((Y5-Y2)*(Z6-Z2)-(Z5-Z2)*(Y6-Y2)) + (Y4-Y2)*((Z5-Z2)*(X6-X2) - (X5-X2)*(Z6-Z2)) + (Z4-Z2)*((X5-X2)*(Y6-Y2) - (Y5-Y2)*(X6-X2))))
  #  Volume_tet3 =  np.abs( (1/6)*( (X3-X2)*((Y4-Y2)*(Z6-Z2)-(Z4-Z2)*(Y6-Y2)) + (Y3-Y2)*((Z4-Z2)*(X6-X2) - (X4-X2)*(Z6-Z2)) + (Z3-Z2)*((X4-X2)*(Y6-Y2) - (Y4-Y2)*(X6-X2))))
  #  Volume_tet4 =  np.abs( (1/6)*( (X4-X3)*((Y6-Y3)*(Z7-Z3)-(Z6-Z3)*(Y7-Y3)) + (Y4-Y3)*((Z6-Z3)*(X7-X3) - (X6-X3)*(Z7-Z3)) + (Z4-Z3)*((X6-X3)*(Y7-Y3) - (Y6-Y3)*(X7-X3))))
  #  Volume_tet5 =  np.abs( (1/6)*( (X5-X4)*((Y6-Y4)*(Z8-Z4)-(Z6-Z4)*(Y8-Y4)) + (Y5-Y4)*((Z6-Z4)*(X8-X4) - (X6-X4)*(Z8-Z4)) + (Z5-Z4)*((X6-X4)*(Y8-Y4) - (Y6-Y4)*(X8-X4))))

 

    ind = np.where(z[10,:,50] >75)
    

    
    fig, ax = plt.subplots()

    ni_volumeV2 = ni_volume[0:18,ind[0][0:-1], 7:110]
    ni_volumeV2 = np.reshape(ni_volumeV2, np.size(ni_volumeV2))
    niN1_volumeV2 = niN1_volume[0:18, ind[0][0:-1], 7:110]
    niN1_volumeV2 = np.reshape(niN1_volumeV2, np.size(niN1_volumeV2))
    niN2_volumeV2 = niN2_volume[0:18,ind[0][0:-1],7:110]
    niN2_volumeV2 = np.reshape(niN2_volumeV2, np.size(niN2_volumeV2))
    niN3_volumeV2 = niN3_volume[0:18,ind[0][0:-1],7:110]
    niN3_volumeV2 = np.reshape(niN3_volumeV2, np.size(niN3_volumeV2))
    niN4_volumeV2 = niN4_volume[0:18, ind[0][0:-1],7:110]
    niN4_volumeV2 = np.reshape(niN4_volumeV2, np.size(niN4_volumeV2))
    niN5_volumeV2 = niN5_volume[0:18, ind[0][0:-1], 7:110]
    niN5_volumeV2 = np.reshape(niN5_volumeV2, np.size(niN5_volumeV2))
    niN6_volumeV2 = niN6_volume[0:18, ind[0][0:-1], 7:110]
    niN6_volumeV2 = np.reshape(niN6_volumeV2, np.size(niN6_volumeV2))
    niN7_volumeV2 = niN7_volume[0:18, ind[0][0:-1], 7:110]
    niN7_volumeV2 = np.reshape(niN7_volumeV2, np.size(niN7_volumeV2))
    Te_volumeV2 = Te_volume[0:18, ind[0][0:-1], 7:110]
    Te_volumeV2 = np.reshape(Te_volumeV2, np.size(Te_volumeV2))

    Cell_volumeV2 = Cell_volume[0:18, ind[0][0:-1], 7:110]
    Cell_volumeV2 = np.reshape(Te_volumeV2, np.size(Te_volumeV2))


    
    ax.pcolormesh(r[0,ind[0],7:-5],z[0, ind[0],7:-5], ni_volume[0,ind[0][0:-1],7:-5])
    plt.show()
    
    Volumelist = []

    #for i in range(0,18):
    #    for j in range(98,223):
    #        for k in range(7,110):
    #             X1 = r[i,j,k] *np.cos(phi[i]*np.pi/180)
    #             Y1 = r[i,j,k] *np.sin(phi[i]*np.pi/180)
    #             Z1 = z[i,j,k]
    
    #             X2 = r[i+1,j,k] * np.cos(phi[i+1]*np.pi/180)
    #             Y2 = r[i+1,j,k] * np.sin(phi[i+1]*np.pi/180)
    #             Z2 = z[i+1,j,k]

        #         X3 = r[i+1, j+1,k] * np.cos(phi[i+1]*np.pi/180)
        #         Y3 = r[i+1,j+1,k]*np.sin(phi[i+1]*np.pi/180)
        #         Z3 = z[i+1,j+1,k]

         #        X4 = r[i, j+1,k] * np.cos(phi[i]*np.pi/180)
         #        Y4 = r[i,j+1,k] * np.sin(phi[i]*np.pi/180)
         #        Z4 = z[i,j+1,k]

    #             X5 = r[i, j, k+1]*np.cos(phi[i]*np.pi/180)
    #             Y5 = r[i,j,k+1] * np.sin(phi[i]*np.pi/180)
    #             Z5 = z[i,j,k+1]

    #             X6 = r[i+1, j, k+1] * np.cos(phi[i+1]*np.pi/180)
    #             Y6 = r[i+1, j, k+1] * np.sin(phi[i+1]*np.pi/180)
    #             Z6 = z[i+1, j, k+1]

    #             X7 = r[i+1, j+1, k+1] * np.cos(phi[i+1]*np.pi/180)
    #             Y7 = r[i+1, j+1, k+1] * np.sin(phi[i+1]*np.pi/180)
    #             Z7 = z[i+1, j+1, k+1]

    #             X8 = r[i, j+1, k+1] * np.cos(phi[0]*np.pi/180)
    #             Y8 = r[i, j+1, k+1] * np.sin(phi[0]*np.pi/180)
    #             Z8 = z[i, j+1, k+1]
    
    #             Volume_tet1 =  np.abs( (1/6)*( (X2-X1)*((Y4-Y1)*(Z5-Z1)-(Z4-Z1)*(Y5-Y1)) + (Y2-Y1)*((Z4-Z1)*(X5-X1) - (X4-X1)*(Z5-Z1)) + (Z2-Z1)*((X4-X1)*(Y5-Y1) - (Y4-Y1)*(X5-X1))))
    #             Volume_tet2 =  np.abs( (1/6)*( (X4-X2)*((Y5-Y2)*(Z6-Z2)-(Z5-Z2)*(Y6-Y2)) + (Y4-Y2)*((Z5-Z2)*(X6-X2) - (X5-X2)*(Z6-Z2)) + (Z4-Z2)*((X5-X2)*(Y6-Y2) - (Y5-Y2)*(X6-X2))))
    #             Volume_tet3 =  np.abs( (1/6)*( (X3-X2)*((Y4-Y2)*(Z6-Z2)-(Z4-Z2)*(Y6-Y2)) + (Y3-Y2)*((Z4-Z2)*(X6-X2) - (X4-X2)*(Z6-Z2)) + (Z3-Z2)*((X4-X2)*(Y6-Y2) - (Y4-Y2)*(X6-X2))))
    #             Volume_tet4 =  np.abs( (1/6)*( (X4-X3)*((Y6-Y3)*(Z7-Z3)-(Z6-Z3)*(Y7-Y3)) + (Y4-Y3)*((Z6-Z3)*(X7-X3) - (X6-X3)*(Z7-Z3)) + (Z4-Z3)*((X6-X3)*(Y7-Y3) - (Y6-Y3)*(X7-X3))))
    #             Volume_tet5 =  np.abs( (1/6)*( (X5-X4)*((Y6-Y4)*(Z8-Z4)-(Z6-Z4)*(Y8-Y4)) + (Y5-Y4)*((Z6-Z4)*(X8-X4) - (X6-X4)*(Z8-Z4)) + (Z5-Z4)*((X6-X4)*(Y8-Y4) - (Y6-Y4)*(X8-X4))))
    #             Tot_volume = Volume_tet1 + Volume_tet2 + Volume_tet3 + Volume_tet4 + Volume_tet5

    #             print("Volume of hexahedral cell", Tot_volume)
    
    #             Volumelist.append(Tot_volume)

    
    Truncatelist = []
    
 
    ntot = niN1+niN2+niN3+niN4+niN5+niN6+niN7
    lower_bound = np.linspace(1.1,15.1,150)
    upper_bound = np.linspace(1.2, 15.2, 150)
    abundancelist1 = []
    abundancelist2 = []
    abundancelist3 = []
    abundancelist4 = []
    abundancelist5 = []
    abundancelist6 = []
    abundancelist7 = []
    abundancesum = []
    
    plt.figure()
    for i in range(len(lower_bound)):

        
        ind = np.where((Te_volumeV2 > lower_bound[i]) & (Te_volumeV2 <upper_bound[i]))
        
        Te_bin1 = np.zeros(1)
        Te_bin2 = np.zeros(1)
        Te_bin3 = np.zeros(1)
        Te_bin4 = np.zeros(1)
        Te_bin5 = np.zeros(1)
        Te_bin6 = np.zeros(1)
        Te_bin7 = np.zeros(1)
        Te_bintot = np.zeros(1)
       
        Ntot = niN1_volumeV2[ind]+niN2_volumeV2[ind]+niN3_volumeV2[ind]+niN4_volumeV2[ind]+niN5_volumeV2[ind]+niN6_volumeV2[ind]+niN7_volumeV2[ind]
        
        
        ind2=np.where(Ntot > 10)

        
        Ntot = Ntot[ind2]
        niN1sub = niN1_volumeV2[ind]
        niN2sub = niN2_volumeV2[ind]
        niN3sub = niN3_volumeV2[ind]
        niN4sub = niN4_volumeV2[ind]
        niN5sub = niN5_volumeV2[ind]
        niN6sub = niN6_volumeV2[ind]
        niN7sub =  niN7_volumeV2[ind]
        Cellsub = Cell_volumeV2[ind]

        
        cN1 = np.divide(niN1sub[ind2],Ntot)
        cN2 = np.divide(niN2sub[ind2],Ntot)
        cN3 = np.divide(niN3sub[ind2],Ntot)
        cN4 = np.divide(niN4sub[ind2],Ntot)
        cN5 = np.divide(niN5sub[ind2],Ntot)
        cN6 = np.divide(niN6sub[ind2],Ntot)
        cN7 = np.divide(niN7sub[ind2],Ntot)

        Volumelist = np.array(Volumelist)
        Te_bin1[0] = np.sum(np.multiply(cN1, Cellsub[ind2]))/np.sum(Cellsub[ind2])
        Te_bin2[0] = np.sum(np.multiply(cN2, Cellsub[ind2]))/np.sum(Cellsub[ind2])
        Te_bin3[0] = np.sum(np.multiply(cN3, Cellsub[ind2]))/np.sum(Cellsub[ind2])
        Te_bin4[0] = np.sum(np.multiply(cN4, Cellsub[ind2]))/np.sum(Cellsub[ind2])
        Te_bin5[0] = np.sum(np.multiply(cN5, Cellsub[ind2]))/np.sum(Cellsub[ind2])
        Te_bin6[0] = np.sum(np.multiply(cN6, Cellsub[ind2]))/np.sum(Cellsub[ind2])
        Te_bin7[0] = np.sum(np.multiply(cN7, Cellsub[ind2]))/np.sum(Cellsub[ind2])
        
        

        abundancelist1.append(Te_bin1)
        abundancelist2.append(Te_bin2)
        abundancelist3.append(Te_bin3)
        abundancelist4.append(Te_bin4)
        abundancelist5.append(Te_bin5)
        abundancelist6.append(Te_bin6)
        abundancelist7.append(Te_bin7)



 
        


   
    plt.scatter(lower_bound, abundancelist1, label='f(N1+)')
    plt.scatter(lower_bound, abundancelist2, label='f(N2+)')
    plt.scatter(lower_bound, abundancelist3, label='f(N3+)')
    plt.scatter(lower_bound, abundancelist4, label='f(N4+)')
    plt.scatter(lower_bound, abundancelist5, label='f(N5+)')
    plt.scatter(lower_bound, abundancelist6, label='f(N6+)')
    plt.scatter(lower_bound, abundancelist7, label='f(N7+)')
    plt.xlabel('Temperature (eV)')
    plt.ylabel('Fractional abundance')
    plt.title('Fractional abundances vs. temperature')
    plt.xscale('log')
    
    plt.legend()
    plt.show() 
   

