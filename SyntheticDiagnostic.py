#I regard this script as the master script, it combines the interpolation from emission line of sights for each spectral line to the contour plot

#It uses the fractional abundance from regions local to the divertor region for the whole toroidal cross section!


import numpy as np
import matplotlib.pyplot as plt
from eelab import Simulation
from eelab import Geometry
import atomdat.adas.adf15 as adf15
import atomdat.adas.adas as adas
from eelab.dtypes import VolData
import matplotlib.cm as cm
from scipy.interpolate import splev
import atomdat.adas.ibal as ibal
from matplotlib.path import Path
from scipy.interpolate import interp2d
from scipy.interpolate import UnivariateSpline
from scipy.interpolate import CubicSpline

#from scipy.interpolate import griddata


#import aurora
from mpl_toolkits.axes_grid1.axes_divider import make_axes_locatable


def get_emission(fn, line,Te,ne, nimp, lim=10,lim2=1E19):
    '''
    Get emission from ADF15 file
    Inputs:
        fn   = name of the adf15 file
        line = idx of line (14 for 3996.13 exc, 114 for 3996.13 rec)
        Te   = array of Te values
        ne   = array of ne values
        nimp = array of impurity density values
        lim  = optional - if lim < Te value, then set emiss to 0
    '''
    mydat=adf15.ADF15()
    mydat.read(fn, v=1)

    mydat.print_line_index([3996,405])
    coeff=mydat.interpolate(ne,Te,line)
    
    emiss=np.multiply(coeff,np.multiply(ne,nimp))
    ind=np.where(Te > lim)
    emiss[ind]=0.
    ind2=np.where(ne <lim2)
    emiss[ind2]=0
    
    
    return emiss

def get_coords(name):
    '''
    Gives coordinates of spectroscopic LoS for different systems
    Options:
    name  -  system name, current options:
             AEI30_OP12b
             AEF30_OP12b
    in the simulations, AEI30 is the same as AEI51 so there is no
    need to specify the 51 ports
    '''
    if name=='AEI30_OP12b':
        coords=np.array([
              [-3.5403017894,3.97650607057,-0.993995223482],
              [-3.54084972951,3.97718784752,-0.98897687935],
              [-3.54139766961,3.97786962448,-0.983958535218],
              [-3.54194560971,3.97855140143,-0.978940191085],
              [-3.54249354981,3.97923317839,-0.973921846953],
              [-3.54304148991,3.97991495534,-0.968903502821],
              [-3.54358943001,3.9805967323,-0.963885158689],
              [-3.54413737012,3.98127850926,-0.958866814557],
              [-3.54468531022,3.98196028621,-0.953848470425],
              [-3.54523325032,3.98264206317,-0.948830126293],
              [-3.54578119042,3.98332384012,-0.943811782161],
              [-3.54632913052,3.98400561708,-0.938793438029],
              [-3.54687707063,3.98468739403,-0.933775093897],
              [-3.54742501073,3.98536917099,-0.928756749765],
              [-3.54797295083,3.98605094794,-0.923738405633],
              [-3.54852089093,3.9867327249,-0.918720061501],
              [-3.54906883103,3.98741450185,-0.913701717369],
              [-3.54961677114,3.98809627881,-0.908683373237],
              [-3.55016471124,3.98877805577,-0.903665029105],
              [-3.55071265134,3.98945983272,-0.898646684973],
              [-3.55126059144,3.99014160968,-0.893628340841],
              [-3.55180853154,3.99082338663,-0.888609996709],
              [-3.55235647165,3.99150516359,-0.883591652576],
              [-3.55290441175,3.99218694054,-0.878573308444],
              [-3.55345235185,3.9928687175,-0.873554964312],
              [-3.55400029195,3.99355049445,-0.86853662018],
              [-3.55454823205,3.99423227141,-0.863518276048]
        ])
        coords*=100
        #original point for line of sight, taken from AEI51
        p0=np.array([0.90812, 2.90156, .62081])*100.
        p0r=np.sqrt(p0[0]*p0[0] + p0[1]*p0[1])
        p0[0]=p0r*np.cos(131.67*np.pi/180)
        p0[1]=p0r*np.sin(131.67*np.pi/180)
        p0[2]*=-1.

    elif name=='AEF30_OP12b':
        coords=np.array([[-3.4519    ,  4.0925    , -1.1609    ],
            [-3.44321355,  4.06895467, -1.14838175],
            [-3.43476654,  4.04549544, -1.13554213],
            [-3.42656076,  4.02212732, -1.12238386],
            [-3.41859798,  3.99885531, -1.10890976],
            [-3.4108799 ,  3.97568438, -1.09512271],
            [-3.40340815,  3.95261948, -1.08102565],
            [-3.39618435,  3.92966554, -1.0666216 ],
            [-3.38921003,  3.90682745, -1.05191363],
            [-3.38248668,  3.88411011, -1.03690488],
            [-3.37601574,  3.86151835, -1.02159857],
            [-3.36979859,  3.83905702, -1.00599796],
            [-3.36383656,  3.8167309 , -0.99010638],
            [-3.35813092,  3.79454477, -0.97392724],
            [-3.3526829 ,  3.77250336, -0.95746397],
            [-3.34749365,  3.75061139, -0.94072011],
            [-3.34256429,  3.72887353, -0.92369923],
            [-3.33789586,  3.70729442, -0.90640496],
            [-3.33348937,  3.68587869, -0.88884101],
            [-3.32934575,  3.66463089, -0.87101112],
            [-3.3254659 ,  3.64355557, -0.85291909],
            [-3.32185064,  3.62265723, -0.83456881],
            [-3.31850073,  3.60194034, -0.81596418],
            [-3.31541691,  3.58140932, -0.79710918],
            [-3.31259983,  3.56106856, -0.77800784],
            [-3.31005008,  3.54092241, -0.75866424],
            [-3.30776821,  3.52097516, -0.73908252]])
        coords*=100
        p0=np.array([-4564., 4472., 356.])/10.

    return coords, p0



#The above get_emission function is descriptive of a line but not a 2D grid. 
ne_array = np.linspace(1.1e19, 0.9e21, 50)
Te_array = np.linspace(3,10,400)
grid = np.zeros((100,100))


fn='pec98#n_ssh_pju#n1.dat'
mydat = adf15.ADF15()
mydat.read(fn, v=1)





length = 100
ne_array2 = np.linspace(1e19, 1e21, 1000)
ne_array3 = np.linspace(1e19, 2e20, 100)
grid2 = np.zeros((1000,1000))
grid3 = np.zeros((1000, 1000))
grid4 = np.zeros((100,100))
grid5 = np.zeros((100, 100))
dummylist = []




#mydat2 = adas.ADAS()
#te, fz, spl = mydat2.coronal(elem='n', pn='~/ADAS/adf11', fplt=False)
N0list = []
N1list = []
N2list = []
N3list = []
N4list = []
N5list = []
N6list = []
N7list = []



for i in range(len(Te_array)):
    MakeArray2 = np.linspace(Te_array[i], Te_array[i], 1)
    te, fz, lz, lzl = ibal.calc_fz(elem='n', ne=(3e19), te = (MakeArray2), pn = '~/ADAS/adf11')
    
    N1list.append(fz[0][1])
    N2list.append(fz[0][2])
    N3list.append(fz[0][3])
    N4list.append(fz[0][4])
    N5list.append(fz[0][5])
    N6list.append(fz[0][6])
    N7list.append(fz[0][7])


plt.figure()
plt.plot(Te_array, N1list, label='f(N1+)')
plt.plot(Te_array, N2list, label='f(N2+)')
plt.plot(Te_array, N3list, label='f(N3+)')
plt.plot(Te_array, N4list, label='f(N4+)')
plt.plot(Te_array, N5list, label='f(N5+)')
plt.plot(Te_array, N6list, label='f(N6+)')
plt.plot(Te_array, N7list, label='f(N7+)')
plt.xscale('log')
plt.show()




if __name__=='__main__':
   #s = Simulation('u/jato/EIM-m286A-Icc500A/P5MW/N3E13D.5_FRAD.8')
    s=Simulation('/u/jato/std/P5MW/N3E13D.5_FRAD.2')
    Te=s['Te']; ni=s['ni']; niN1=s['niN1']; niN2 = s['niN2']; Ti=s['Ti']; niN3=s['niN3']; niN4 = s['niN4']; niN5 = s['niN5']; niN6 = s['niN6']; niN7 = s['niN7']

    g=Geometry('/u/jato/std/geometry')

    phi = g.phi_plane
    ntor = g.srf_toro[0]; npol=g.srf_polo[0]; nrad=g.srf_radi[0]
    r = np.reshape(g.rg, (ntor, npol, nrad))
    z = np.reshape(g.zg, (ntor, npol, nrad))

    x=np.zeros(np.shape(r)); y=np.zeros(np.shape(r))
    
    for i in range(len(phi)):
        x[i,:,:]=np.multiply(r[i,:,:],np.cos(phi[i]*np.pi/180))
        y[i,:,:]=np.multiply(r[i,:,:],np.sin(phi[i]*np.pi/180))
        
        my_volcel=np.zeros((ntor-1,npol-1,nrad-1))
        def tetrahedron_volume(v1,v2,v3,v4):
            matrix=np.array([
                [v1[0], v1[1], v1[2],1],
                [v2[0], v2[1], v2[2],1],
                [v3[0], v3[1], v3[2],1],
                [v4[0], v4[1], v4[2],1]])
            det=np.linalg.det(matrix)
            volume=abs(det)/6
            return volume
        
    #Step 2: Calculate cell volumes
    for i in range(ntor-1):
        print('Started toroidal index %i' %i)
            
        for j in range(npol-1):
            for k in range(nrad-1):
                x1=x[i,j,k]; x2=x[i+1,j,k]; x3=x[i+1,j+1,k]; x4=x[i,j+1,k]; x5=x[i,j,k+1]; x6=x[i+1,j,k+1]; x7=x[i+1,j+1,k+1]; x8=x[i,j+1,k+1]
                y1=y[i,j,k]; y2=y[i+1,j,k]; y3=y[i+1,j+1,k]; y4=y[i,j+1,k]; y5=y[i,j,k+1]; y6=y[i+1,j,k+1]; y7=y[i+1,j+1,k+1]; y8=y[i,j+1,k+1]
                z1=z[i,j,k]; z2=z[i+1,j,k]; z3=z[i+1,j+1,k]; z4=z[i,j+1,k]; z5=z[i,j,k+1]; z6=z[i+1,j,k+1]; z7=z[i+1,j+1,k+1]; z8=z[i,j+1,k+1]
                    
                v1=np.array([x1,y1,z1]); v2=np.array([x2,y2,z2]); v3=np.array([x3,y3,z3]); v4=np.array([x4,y4,z4])
                v5=np.array([x5,y5,z5]); v6=np.array([x6,y6,z6]); v7=np.array([x7,y7,z7]); v8=np.array([x8,y8,z8])

                 #tetrahedron 1: indices 1, 2, 4, and 5
                vt1=tetrahedron_volume(v1,v2,v4,v5)
                    
                #tetrahedron 2: indices 2, 3, 4 and 7
                vt2=tetrahedron_volume(v2,v3,v4,v7)
                    
                #tetrahedron 3: indices 2, 4, 5, and 7
                vt3=tetrahedron_volume(v2,v4,v5,v7)
                    
                #tetrahedron 4: indices 5, 6, 7, and 2
                vt4=tetrahedron_volume(v5,v6,v7,v2)
                    
                #tetrahedron 5: indices 4, 5, 7, and 8
                vt5=tetrahedron_volume(v4,v5,v7,v8)

                my_volcel[i,j,k]=vt1+vt2+vt3+vt4+vt5

    idcell = ni.grid.idcell
    ind=np.where(idcell < ni.grid.nc_pl)

    ni_volume=np.zeros(np.shape(idcell))
    ni_volume[ind] = ni.values[idcell[ind]]
    ni_volume = np.reshape(ni_volume,(ntor-1, npol-1, nrad-1))
    niN1_volume=np.zeros(np.shape(idcell))
    niN1_volume[ind] = niN1.values[idcell[ind]]
    niN1_volume = np.reshape(niN1_volume, (ntor-1, npol-1,nrad-1))
    niN2_volume=np.zeros(np.shape(idcell))
    niN2_volume[ind] = niN2.values[idcell[ind]]
    niN2_volume=np.reshape(niN2_volume, (ntor-1, npol-1, nrad-1))
    niN3_volume=np.zeros(np.shape(idcell))
    niN3_volume[ind] = niN3.values[idcell[ind]]
    niN3_volume=np.reshape(niN3_volume, (ntor-1, npol-1, nrad-1))
    niN4_volume=np.zeros(np.shape(idcell))
    niN4_volume[ind] = niN4.values[idcell[ind]]
    niN4_volume = np.reshape(niN4_volume, (ntor-1, npol-1, nrad-1))
    niN5_volume=np.zeros(np.shape(idcell))
    niN5_volume[ind] = niN5.values[idcell[ind]]
    niN5_volume = np.reshape(niN5_volume, (ntor-1, npol-1, nrad-1))
    niN6_volume=np.zeros(np.shape(idcell))
    niN6_volume[ind] = niN6.values[idcell[ind]]
    niN6_volume = np.reshape(niN6_volume, (ntor-1, npol-1, nrad-1))
    niN7_volume=np.zeros(np.shape(idcell))
    niN7_volume[ind] = niN7.values[idcell[ind]]
    niN7_volume= np.reshape(niN7_volume, (ntor-1, npol-1, nrad-1))
    Te_volume = np.zeros(np.shape(idcell))
    Te_volume[ind] = Te.values[idcell[ind]]
    Te_volume = np.reshape(Te_volume, (ntor-1, npol-1, nrad-1))


    Cell_volume = np.zeros(np.shape(idcell))
    Cell_volume = np.reshape(my_volcel, (ntor-1, npol-1, nrad-1))


    ind = np.where(z[10,:,50] >75)
    

    
    fig, ax = plt.subplots()

    ni_volumeV2 = ni_volume[0:18,ind[0][0:-1], 7:110]
    ni_volumeV2 = np.reshape(ni_volumeV2, np.size(ni_volumeV2))
    niN1_volumeV2 = niN1_volume[0:18, ind[0][0:-1], 7:110]
    niN1_volumeV2 = np.reshape(niN1_volumeV2, np.size(niN1_volumeV2))
    niN2_volumeV2 = niN2_volume[0:18,ind[0][0:-1],7:110]
    niN2_volumeV2 = np.reshape(niN2_volumeV2, np.size(niN2_volumeV2))
    niN3_volumeV2 = niN3_volume[0:18,ind[0][0:-1],7:110]
    niN3_volumeV2 = np.reshape(niN3_volumeV2, np.size(niN3_volumeV2))
    niN4_volumeV2 = niN4_volume[0:18, ind[0][0:-1],7:110]
    niN4_volumeV2 = np.reshape(niN4_volumeV2, np.size(niN4_volumeV2))
    niN5_volumeV2 = niN5_volume[0:18, ind[0][0:-1], 7:110]
    niN5_volumeV2 = np.reshape(niN5_volumeV2, np.size(niN5_volumeV2))
    niN6_volumeV2 = niN6_volume[0:18, ind[0][0:-1], 7:110]
    niN6_volumeV2 = np.reshape(niN6_volumeV2, np.size(niN6_volumeV2))
    niN7_volumeV2 = niN7_volume[0:18, ind[0][0:-1], 7:110]
    niN7_volumeV2 = np.reshape(niN7_volumeV2, np.size(niN7_volumeV2))
    Te_volumeV2 = Te_volume[0:18, ind[0][0:-1], 7:110]
    Te_volumeV2 = np.reshape(Te_volumeV2, np.size(Te_volumeV2))

    Cell_volumeV2 = Cell_volume[0:18, ind[0][0:-1], 7:110]
    Cell_volumeV2 = np.reshape(Te_volumeV2, np.size(Te_volumeV2))

    
    Volumelist = []

    Truncatelist = []

    ntot = niN1+niN2+niN3+niN4+niN5+niN6+niN7
    lower_bound = np.linspace(1.1,15.1,150)
    upper_bound = np.linspace(1.2, 15.2, 150)
    abundancelist1 = []
    abundancelist2 = []
    abundancelist3 = []
    abundancelist4 = []
    abundancelist5 = []
    abundancelist6 = []
    abundancelist7 = []
    abundancesum = []

    
    
   # plt.figure()
    for i in range(len(lower_bound)):

        
        ind = np.where((Te_volumeV2 > lower_bound[i]) & (Te_volumeV2 <upper_bound[i]))
        
        Te_bin1 = np.zeros(1)
        Te_bin2 = np.zeros(1)
        Te_bin3 = np.zeros(1)
        Te_bin4 = np.zeros(1)
        Te_bin5 = np.zeros(1)
        Te_bin6 = np.zeros(1)
        Te_bin7 = np.zeros(1)
        Te_bintot = np.zeros(1)
       
        Ntot = niN1_volumeV2[ind]+niN2_volumeV2[ind]+niN3_volumeV2[ind]+niN4_volumeV2[ind]+niN5_volumeV2[ind]+niN6_volumeV2[ind]+niN7_volumeV2[ind]
        
        
        ind2=np.where(Ntot > 10)

        
        Ntot = Ntot[ind2]
        niN1sub = niN1_volumeV2[ind]
        niN2sub = niN2_volumeV2[ind]
        niN3sub = niN3_volumeV2[ind]
        niN4sub = niN4_volumeV2[ind]
        niN5sub = niN5_volumeV2[ind]
        niN6sub = niN6_volumeV2[ind]
        niN7sub =  niN7_volumeV2[ind]
        Cellsub = Cell_volumeV2[ind]

        
        cN1 = np.divide(niN1sub[ind2],Ntot)
        cN2 = np.divide(niN2sub[ind2],Ntot)
        cN3 = np.divide(niN3sub[ind2],Ntot)
        cN4 = np.divide(niN4sub[ind2],Ntot)
        cN5 = np.divide(niN5sub[ind2],Ntot)
        cN6 = np.divide(niN6sub[ind2],Ntot)
        cN7 = np.divide(niN7sub[ind2],Ntot)

        Volumelist = np.array(Volumelist)
        Te_bin1[0] = np.sum(np.multiply(cN1, Cellsub[ind2]))/np.sum(Cellsub[ind2])
        Te_bin2[0] = np.sum(np.multiply(cN2, Cellsub[ind2]))/np.sum(Cellsub[ind2])
        Te_bin3[0] = np.sum(np.multiply(cN3, Cellsub[ind2]))/np.sum(Cellsub[ind2])
        Te_bin4[0] = np.sum(np.multiply(cN4, Cellsub[ind2]))/np.sum(Cellsub[ind2])
        Te_bin5[0] = np.sum(np.multiply(cN5, Cellsub[ind2]))/np.sum(Cellsub[ind2])
        Te_bin6[0] = np.sum(np.multiply(cN6, Cellsub[ind2]))/np.sum(Cellsub[ind2])
        Te_bin7[0] = np.sum(np.multiply(cN7, Cellsub[ind2]))/np.sum(Cellsub[ind2])
        
        

        abundancelist1.append(Te_bin1)
        abundancelist2.append(Te_bin2)
        abundancelist3.append(Te_bin3)
        abundancelist4.append(Te_bin4)
        abundancelist5.append(Te_bin5)
        abundancelist6.append(Te_bin6)
        abundancelist7.append(Te_bin7)

    abundancelist1 = np.array(abundancelist1)
    abundancelist2 = np.array(abundancelist2)
    abundancelist3 = np.array(abundancelist3)
    abundancelist4 = np.array(abundancelist4)
    abundancelist5 = np.array(abundancelist5)
    abundancelist6 = np.array(abundancelist6)
    abundancelist7 = np.array(abundancelist7)
    print("length of abundancelist1", len(abundancelist1))
    print("length of lower_bound", len(lower_bound))


    ##IMPORTANT: Smoothing parameter for spline fits to be adjusted for each different simulation to give the best fit :)
    
    spline1 = UnivariateSpline(lower_bound, abundancelist1, s=0.1)
    spline2 = UnivariateSpline(lower_bound, abundancelist2, s=0.05)
    spline3 = UnivariateSpline(lower_bound, abundancelist3, s=0.05)
    spline4 = UnivariateSpline(lower_bound, abundancelist4, s=0.05)
    spline5 = UnivariateSpline(lower_bound, abundancelist5, s=0.05)
    spline6 = UnivariateSpline(lower_bound, abundancelist6, s=0.05)
    spline7 = UnivariateSpline(lower_bound, abundancelist7, s=0.05)
    
        
    x_dense = np.linspace(1,9,1000)
    y_dense1 = spline1(x_dense)
    y_dense2 = spline2(x_dense)
    y_dense3 = spline3(x_dense)
    y_dense4 = spline4(x_dense)
    y_dense5 = spline5(x_dense)
    y_dense6 = spline6(x_dense)
    y_dense7 = spline7(x_dense)

    plt.figure()
    plt.plot(x_dense, y_dense1)
    plt.plot(x_dense, y_dense2)
    plt.plot(x_dense, y_dense3)
    plt.plot(x_dense, y_dense4)
    plt.plot(x_dense, y_dense5)
    plt.plot(x_dense, y_dense6)
    plt.plot(x_dense, y_dense7)

    
    
    plt.scatter(lower_bound, abundancelist1, label='f(N1+)')
    plt.scatter(lower_bound, abundancelist2, label='f(N2+)')
    plt.scatter(lower_bound, abundancelist3, label='f(N3+)')
    plt.scatter(lower_bound, abundancelist4, label='f(N4+)')
    plt.scatter(lower_bound, abundancelist5, label='f(N5+)')
    plt.scatter(lower_bound, abundancelist6, label='f(N6+)')
    plt.scatter(lower_bound, abundancelist7, label='f(N7+)')
    plt.xlabel('Temperature (eV)')
    plt.ylabel('Fractional abundance')
    plt.title('Fractional abundances vs. temperature')
    plt.xscale('log')
    
    plt.legend()

    plt.show()

    x_fine = np.linspace(1,8,10000)
    y_fine = np.interp(x_fine, x_dense, y_dense1)

    
    BigGrid1 = np.zeros((1000,1000))
    BigGrid2 = np.zeros((1000,1000))

    for i in range(len(x_dense)):
        MakeArray2 = np.linspace(x_dense[i], x_dense[i], 1)
        for j in range(len(ne_array2)):
            MakeArray = np.linspace(ne_array2[j], ne_array2[j], 1)
            result = mydat.interpolate(MakeArray, MakeArray2, 14)
            result2 = mydat.interpolate(MakeArray, MakeArray2, 20)
            resultrec = mydat.interpolate(MakeArray, MakeArray2, 64)
            result2rec = mydat.interpolate(MakeArray, MakeArray2, 70)

            BigGrid1[j][i] = (y_dense1[i]*result2+y_dense2[i]*result2rec)/(y_dense1[i]*result+y_dense2[i]*resultrec)
 
    for i in range(len(x_dense)):
        MakeArray2 = np.linspace(x_dense[i], x_dense[i], 1)
        for j in range(len(ne_array2)):
            MakeArray = np.linspace(ne_array2[j], ne_array2[j], 1)
            result = mydat.interpolate(MakeArray, MakeArray2, 18)
            result2 = mydat.interpolate(MakeArray, MakeArray2, 20)
            resultrec = mydat.interpolate(MakeArray, MakeArray2, 68)
            result2rec = mydat.interpolate(MakeArray, MakeArray2, 70)

            BigGrid2[j][i] = (y_dense1[i]*result2 + y_dense2[i]*result2rec)/(y_dense1[i]*result+y_dense2[i]*resultrec)

        
 

        
    plt.figure()

    contours1 = plt.contour(x_dense, ne_array2, BigGrid1, label = '404.1/399.5', levels=20)
    contours2 = plt.contour(x_dense, ne_array2, BigGrid2, label = '404.1/402.6', levels=20)
    plt.clabel(contours1, inline=True, fontsize=8)
    plt.clabel(contours2, inline=True, fontsize=8)
    plt.yscale('log')
    plt.title('Calibration plot')
    plt.show()


    Weighted_Ts = []
    Weighted_ns = []
    Interpolated_Ts = []
    Interpolated_ns = []


   # if __name__=='__main__':
   # s=Simulation('/u/jato/std/P5MW/N3E13D.2_SP4_CX_N')
   # s=Simulation('/u/jato/std/P5MW/N3E13D.5_FRAD.2')
    
    # load needed parameters 
   # Te=s['Te']; ni=s['ni']; niN1=s['niN1']; niN2=s['niN2']; Ti=s['Ti']
   
    
   
    fn='pec98#n_ssh_pju#n1.dat' 
    emissVolData = VolData(grid=Te.grid, values = get_emission(fn,14,Te.values, ni.values*1e6, niN1.values*1e6) + get_emission(fn, 64, Te.values, ni.values*1e6, niN2.values*1e6), symbol='Emissivity', units='J/s/m^2', rank='scalar')
    emissVolData.plot('rzslice', 12)
                    
    #generate VolData for N1 concentration
    Cn=VolData(grid=Te.grid, values=np.divide(niN1.values,ni.values), symbol='Cn', units='None', rank='scalar')
    
    spec_system='AEI30_OP12b'
    coords, p0 = get_coords(spec_system)
    sh=np.shape(coords)
   # print("shape of coords", sh)
    #spec_res = line integrated emission for each LoS
    spec_res=np.zeros((sh[0],))
   # print("spec_res initial", spec_res)
    mask=np.zeros(np.shape(spec_res))
   # print("mask initial", mask)
    #Te_weight = emission weighted Te along LoS
    
    Te_weight=np.zeros(np.shape(spec_res))
    #ni_weight = emission weighted ni along LoS
    ni_weight=np.zeros(np.shape(spec_res))
    #cn1_weight = emission weighted N1 concentration along LoS
    cn1_weight=np.zeros(np.shape(spec_res))


   
    
    for i in range(sh[0]-10):
 
        
        #get LoS vector from spectrometer coordinates
        
        d=coords[i,:] - p0
        
        mag=0.
        for j in range(len(d)):
            mag+=d[j]*d[j]
        mag=np.sqrt(mag)
        v=d/mag
        
        
        #---------------------------------------------------------------
        #map the line of sight geometry
        #mapped[0] = LoS object
        #            LoS.gc - grouped cell centers = len(imap)
        #            LoS.gt - grouped cell vertices = len(imap)+1
        #mapped[1] = imap (eelab indices of VolData values along LoS)
        #            VolDataObj.get_values(imap) gets you the data
        #---------------------------------------------------------------
        
        if True:
            
            mapped=s.geometry.map_line_of_sight(p0, v, groupby_idcell=True)
           # print("mapped[0].s]",len(mapped[0].s))
           # print("mapped[1].s]", mapped[1])
           # print('Length of gc: %i' %(len(mapped[0].gc)))
           # print('Length of gt: %i' %(len(mapped[0].gt)))
        
            
            #---------------------------------------------------------------
            #Get differential distance along LoS for line integration
            #|----|----|----|----|----|----|
            #  d0   d1   d2   d3   d4   d5  
            #---------------------------------------------------------------
           
            x=mapped[0].x1
          
            y=mapped[0].x2

            z=mapped[0].x3
         
            r=np.sqrt(np.multiply(x,x) + np.multiply(y,y))
            phi = np.arctan2(y,x)*180/np.pi
            phi = np.abs(phi-2*72)
            Te_data = np.zeros((len(mapped[0].s),))
            
                            
            
            
            plt.plot(r,z, color='red')
            
            
            d=np.zeros((len(mapped[0].s[1:-1])-2,))  
           # print("length of d array",len(d))
            for j in range(len(mapped[0].s[1:-1])-2):
                d[j]=mapped[0].s[j+2]-mapped[0].s[j+1]
            
            mapped_new = mapped[0].s[1:-1]

         
            
            
            #---------------------------------------------------------------
            #Get emission along the LoS (from Aurora)
            #for Nitrogen, the indices are:
            #14 - excitation of N1
            #64 - recombination of N2
            #---------------------------------------------------------------
            
           
            Te1d=np.array(Te.get_values(mapped[1])); ni1d=np.array(ni.get_values(mapped[1]))
            niN11d=np.array(niN1.get_values(mapped[1])); niN21d=np.array(niN2.get_values(mapped[1]))
            Cn1d=np.array(Cn.get_values(mapped[1]))
            Emiss1d=np.array(emissVolData.get_values(mapped[1]))
            # Debugging for AEF line of sight



            
            ind=np.isnan(Cn1d)
            Cn1d[ind]=0.
            #excitation emission
            emiss1d = np.array(get_emission(fn, 14, Te1d, ni1d*1e6, niN11d*1e6) + get_emission(fn, 64, Te1d, ni1d*1e6, niN11d*1e6))
            emiss1d399 = np.array(get_emission(fn, 14, Te1d, ni1d*1e6, niN11d*1e6) + get_emission(fn, 64, Te1d, ni1d*1e6, niN11d*1e6))
            emiss1d402 = np.array(get_emission(fn, 18, Te1d, ni1d*1e6, niN11d*1e6) + get_emission(fn, 68, Te1d, ni1d*1e6, niN11d*1e6))
            emiss1d404 = np.array(get_emission(fn, 20, Te1d, ni1d*1e6, niN11d*1e6) + get_emission(fn, 70, Te1d, ni1d*1e6, niN11d*1e6))

            

            #I need to do temperature weighted emission for each and compare to that found through the ratio of emissions method
            
            
            
            line_399 = 0
            line_404 = 0
            line_402 = 0
            for i in range(len(d)):
                line_399 += emiss1d399[i]
                line_404 += emiss1d404[i]
                line_402 += emiss1d402[i] 
            print("the ratio of 404 to 399 is:", line_404/line_399)
            print("The ratio of 404 to 402 is:", line_404/line_402)
            print("The ratio of 402 to 399 is:", line_402/line_399)

            # Here I am going to calculate the temperature and density weighted values

            Top_integral_result_T = 0
            Bottom_integral_result_T = 0
            Top_integral_result_n = 0
            Bottom_integral_result_n = 0

            for i in range(len(d)):
                Top_integral_result_T += Te1d[i]*emiss1d[i]
                Bottom_integral_result_T += emiss1d[i]
                Top_integral_result_n += ni1d[i]*emiss1d[i]
                Bottom_integral_result_n += emiss1d[i]

            if Bottom_integral_result_T != 0:
                print("Temperature weighted emission", Top_integral_result_T/Bottom_integral_result_T)
                Weighted_Ts.append(Top_integral_result_T/Bottom_integral_result_T)
            if Bottom_integral_result_n != 0:
                print("Density weighted emission", Top_integral_result_n/Bottom_integral_result_n)
                Weighted_ns.append(Top_integral_result_n/Bottom_integral_result_n)    

            def get_contour_paths(contour):
                paths = []
                for collection in contour.collections:
                    for path in collection.get_paths():
                        paths.append(path)
                    return paths


            paths1 = get_contour_paths(contours1)
            paths2 = get_contour_paths(contours2)

            def find_intersection_points(paths1, paths2):
                intersection_points = []
                for path1 in paths1:
                    for path2 in paths2:
                        for p1, _ in path1.iter_segments():
                            for p2, _ in path2.iter_segments():
                                if np.allclose(p1, p2, atol=1e-2):
                                    intersection_points.append(p1)
                return intersection_points

            intersection_points = find_intersection_points(paths1, paths2)

            intersection_x = [point[0] for point in intersection_points]
            intersection_y = [point[1] for point in intersection_points]

            for i, (x_val, y_val) in enumerate(zip(intersection_x, intersection_y)):
                print(f"Intersection point {i+1}: x = {x_val}, y = {y_val}")
            

            
                
            Ratio = line_404/line_399
            Ratio2 = line_404/line_402
      
            tolerance = 0.001
            tolerance2 = 0.01
            matching_points = []
            matching_points2 = []
            matching_pointsT = []
            matching_pointsn = []
            matching_points2n = []
            matching_points2T = []

           # print(grid_fine1)
            
            for i in range(1000):
                for j in range(1000):
                    if abs(BigGrid1[i,j] - Ratio) < tolerance:
                        matching_points.append((x_dense[j], ne_array2[i], BigGrid1[i,j]))
                        matching_pointsT.append(x_dense[j])
                        matching_pointsn.append(ne_array2[i])

            print("this is the number of matching points", len(matching_points))
            for i in range(1000):
                for j in range(1000):
                    if abs(BigGrid2[i,j] - Ratio2) < tolerance2:
                        matching_points2T.append(x_dense[j])
                        matching_points2n.append(ne_array2[i])
                        matching_points2.append((x_dense[j], ne_array2[i], BigGrid2[i,j]))
          
            plt.figure()
            contours1 = plt.contour(x_dense, ne_array2, BigGrid1, levels = 20)
            contours2 = plt.contour(x_dense, ne_array2, BigGrid2, levels = 20)
            plt.clabel(contours1, inline=True, fontsize=8)
            plt.clabel(contours2, inline=True, fontsize=8)
            plt.scatter(matching_pointsT, matching_pointsn)
            plt.scatter(matching_points2T, matching_points2n)
            plt.yscale('log')
            plt.show()
            def find_closest_point_pair(points1, points2):
                min_distance = float('inf')
                closest_pair = (None, None)

                for p1 in points1:
                    for p2 in points2:
                        dist = np.sqrt((p1[0] - p2[0])**2 + (p1[1] - p2[1])**2)
                        if dist < min_distance:
                            min_distance = dist
                            closest_pair = (p1, p2)
                return closest_pair

            closest_pair = find_closest_point_pair(matching_points, matching_points2)

            if closest_pair[0] is not None and closest_pair[1] is not None:
                p1, p2 = closest_pair
                text_to_display = f"Closest intersection pair: Contour1 (x = {p1[0]}, y = {p1[1]}, z = {p1[2]} Contour2 (x = {p2[0]}, y = {p2[1]}, z = {p2[2]}"
                                 

                fig, ax = plt.subplots(figsize=(6, 4))

                ax.axis('off')
                plt.text(0.5, 0.5, text_to_display, fontsize=12, ha='center', va='center')
                plt.show()
   
                Interpolated_Ts.append(p1[0])
                Interpolated_ns.append(p1[1])
            else:
                print("No intersection points found")

            def find_closest_points(points1, points2):
                closest_pairs = []
                for p1 in points1:
                    closest_point = None
                    min_distance = float('inf')
                    for p2 in points2:
                        distance = np.sqrt((p1[0] - p2[0])**2+ (p1[1]-p2[1])**2)
                        if distance < min_distance:
                            min_distance = distance
                            closest_point = p2
                    if closest_point is not None:
                        closest_pairs.append((p1, closest_point))
                return closest_pairs
        

            # This part will attempt to obtain total ion density from a crossover on the contour plot
            # The PEC will be found from the density and temperature
            # The fractional abundance of N1+ will be found from the temperature by using a fine 1D plot
            # The emission will be found by summing along the line of sight


            # Do the emission first!
            EmissionResult = 0
            for i in range(len(d)):
                EmissionResult += emiss1d[i]
            print("This is the Emission result!", EmissionResult)
            print("This is the density I need!", p1[1])
            #Now I need the excitation PEC for the 399.5 nm line

            DensArray = np.linspace(p1[1], p1[1], 1)
            TempArray = np.linspace(p1[0], p1[0], 1)
            PECexc399 = mydat.interpolate(DensArray, TempArray, 14)
            print("This is the PEC!", PECexc399)

            closest_index = np.argmin(np.abs(x_fine-p1[0]))
            y_closest = y_fine[closest_index]
            print("Frac Abundance value", y_closest)
            #y_closest represents the fractional abundance of N1+ at the temperature interpolated from the contour plot
            Ntot = EmissionResult/(p1[1]*y_closest*PECexc399)

            print("Ntot", Ntot)

            

            
plt.figure()
plt.scatter(np.array(Weighted_Ts), 1e6*np.array(Weighted_ns), label='Emission weighted from line of sight')
plt.scatter(Interpolated_Ts, Interpolated_ns, label='Temperature and density derived from line ratio')
plt.legend()
plt.show()
