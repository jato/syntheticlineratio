import numpy as np
import matplotlib.pyplot as plt
from eelab import Simulation
import atomdat.adas.adf15 as adf15
import atomdat.adas.ibal as ibal
from eelab.dtypes import VolData
import matplotlib.cm as cm
#import aurora
from mpl_toolkits.axes_grid1.axes_divider import make_axes_locatable


def get_emission(fn, line,Te,ne, nimp, lim=10,lim2=1E19):
    '''
    Get emission from ADF15 file
    Inputs:
        fn   = name of the adf15 file
        line = idx of line (14 for 3996.13 exc, 114 for 3996.13 rec)
        Te   = array of Te values
        ne   = array of ne values
        nimp = array of impurity density values
        lim  = optional - if lim < Te value, then set emiss to 0
    '''
    mydat=adf15.ADF15()
    mydat.read(fn, v=1)
    
    #print(mydat.dat.wvl[14])
   # print(mydat.dat.wvl[64])
    mydat.print_line_index([3996,405])
    coeff=mydat.interpolate(ne,Te,line)
    
    emiss=np.multiply(coeff,np.multiply(ne,nimp))
    ind=np.where(Te > lim)
    emiss[ind]=0.
    ind2=np.where(ne <lim2)
    emiss[ind2]=0
    #print("the coefficient is", coeff)
    
    return emiss

#print("trial get_emission", get_emission('pec98#n_ssh_pju#n1.dat', 14, 5, 2e19 )) 


def get_coeff(fn, line, Te, ne, nimp, lim=10, lim2=1E19):
    mydat = adf15.ADF15('pec98#n_ssh_pju#n1.dat')
    mydat.read(fn, v=1)
    mydat.print_line_index([3996, 405])
    coeff=mydat.interpolate(ne,Te,line)
    return coeff




#The above get_emission function is descriptive of a line but not a 2D grid. 
ne_array = np.linspace(1.1e19, 0.9e21, 100)
Te_array = np.linspace(3,10,100)
grid = np.zeros((100,100))


#fn='pec98#n_ssh_pju#n1.dat'
fn='pec96#n_vsu#n1.dat'
mydat = adf15.ADF15()
mydat.read(fn, v=1)


length = 100
ne_array2 = np.linspace(1e19, 1e19, 100)
grid2 = np.zeros((100,100))

dummylist = []
#s=Simulation('/u/jato/EIM-m286A-Icc500A/P5MW/N3E13D.5_FRAD.8')
    
    # load needed parameters
#Te=s['Te']; ni=s['ni']; niN1=s['niN1']; niN2=s['niN2']; Ti=s['Ti']



def get_coords(name):
    '''
    Gives coordinates of spectroscopic LoS for different systems
    Options:
    name  -  system name, current options:
             AEI30_OP12b
             AEF30_OP12b
    in the simulations, AEI30 is the same as AEI51 so there is no
    need to specify the 51 ports
    '''
    if name=='AEI30_OP12b':
        coords=np.array([
              [-3.5403017894,3.97650607057,-0.993995223482],
              [-3.54084972951,3.97718784752,-0.98897687935],
              [-3.54139766961,3.97786962448,-0.983958535218],
              [-3.54194560971,3.97855140143,-0.978940191085],
              [-3.54249354981,3.97923317839,-0.973921846953],
              [-3.54304148991,3.97991495534,-0.968903502821],
              [-3.54358943001,3.9805967323,-0.963885158689],
              [-3.54413737012,3.98127850926,-0.958866814557],
              [-3.54468531022,3.98196028621,-0.953848470425],
              [-3.54523325032,3.98264206317,-0.948830126293],
              [-3.54578119042,3.98332384012,-0.943811782161],
              [-3.54632913052,3.98400561708,-0.938793438029],
              [-3.54687707063,3.98468739403,-0.933775093897],
              [-3.54742501073,3.98536917099,-0.928756749765],
              [-3.54797295083,3.98605094794,-0.923738405633],
              [-3.54852089093,3.9867327249,-0.918720061501],
              [-3.54906883103,3.98741450185,-0.913701717369],
              [-3.54961677114,3.98809627881,-0.908683373237],
              [-3.55016471124,3.98877805577,-0.903665029105],
              [-3.55071265134,3.98945983272,-0.898646684973],
              [-3.55126059144,3.99014160968,-0.893628340841],
              [-3.55180853154,3.99082338663,-0.888609996709],
              [-3.55235647165,3.99150516359,-0.883591652576],
              [-3.55290441175,3.99218694054,-0.878573308444],
              [-3.55345235185,3.9928687175,-0.873554964312],
              [-3.55400029195,3.99355049445,-0.86853662018],
              [-3.55454823205,3.99423227141,-0.863518276048]
        ])
        coords*=100
        #original point for line of sight, taken from AEI51
        p0=np.array([0.90812, 2.90156, .62081])*100.
        p0r=np.sqrt(p0[0]*p0[0] + p0[1]*p0[1])
        p0[0]=p0r*np.cos(131.67*np.pi/180)
        p0[1]=p0r*np.sin(131.67*np.pi/180)
        p0[2]*=-1.

    elif name=='AEF30_OP12b':
        coords=np.array([[-3.4519    ,  4.0925    , -1.1609    ],
            [-3.44321355,  4.06895467, -1.14838175],
            [-3.43476654,  4.04549544, -1.13554213],
            [-3.42656076,  4.02212732, -1.12238386],
            [-3.41859798,  3.99885531, -1.10890976],
            [-3.4108799 ,  3.97568438, -1.09512271],
            [-3.40340815,  3.95261948, -1.08102565],
            [-3.39618435,  3.92966554, -1.0666216 ],
            [-3.38921003,  3.90682745, -1.05191363],
            [-3.38248668,  3.88411011, -1.03690488],
            [-3.37601574,  3.86151835, -1.02159857],
            [-3.36979859,  3.83905702, -1.00599796],
            [-3.36383656,  3.8167309 , -0.99010638],
            [-3.35813092,  3.79454477, -0.97392724],
            [-3.3526829 ,  3.77250336, -0.95746397],
            [-3.34749365,  3.75061139, -0.94072011],
            [-3.34256429,  3.72887353, -0.92369923],
            [-3.33789586,  3.70729442, -0.90640496],
            [-3.33348937,  3.68587869, -0.88884101],
            [-3.32934575,  3.66463089, -0.87101112],
            [-3.3254659 ,  3.64355557, -0.85291909],
            [-3.32185064,  3.62265723, -0.83456881],
            [-3.31850073,  3.60194034, -0.81596418],
            [-3.31541691,  3.58140932, -0.79710918],
            [-3.31259983,  3.56106856, -0.77800784],
            [-3.31005008,  3.54092241, -0.75866424],
            [-3.30776821,  3.52097516, -0.73908252]])
        coords*=100
        p0=np.array([-4564., 4472., 356.])/10.

    return coords, p0

dummylistne = []
dummylistTe = []

if __name__=='__main__':

   # s=Simulation('/u/jato/EIM-m286A-Icc500A/P5MW/N3E13D.5_FRAD.8')
   # s=Simulation('/u/jato/std/P5MW/N2E13D.5_FRAD.8')
    s = Simulation('/u/jato/std/P5MW/N4E13D.5_FRAD.8')
    # load needed parameters 
    Te=s['Te']; ni=s['ni']; niN1=s['niN1']; niN2=s['niN2']; Ti=s['Ti']; niN3 = s['niN3'];niN4=s['niN4'];niN5=s['niN5'];niN6=s['niN6'];niN7=s['niN7']
  
    fn='pec96#n_vsu#n1.dat' 
    emissVolData = VolData(grid=Te.grid, values = get_emission(fn,1,Te.values, ni.values*1e6, niN1.values*1e6) + get_emission(fn, 51, Te.values, ni.values*1e6, niN2.values*1e6), symbol='Emissivity', units='J/s/m^2', rank='scalar')
    #emissVolData.plot('rzslice', 12)

    n_totalnitrogen = ni.values*0.02

    T_range = np.linspace(0.1, 20, 1000)
    Density = np.linspace(3e19, 3e19, 1)
    F1 = np.zeros(len(T_range))
    F2 = np.zeros(len(T_range))

    for i in range(len(T_range)):
        MakeArray = np.linspace(T_range[i], T_range[i], 1)
        te, fz, lz, lzl = ibal.calc_fz(elem='n', ne=(Density), te=(MakeArray), pn='~/ADAS/adf11')
        F1[i] = fz[0][1]
        F2[i] = fz[0][2]

    T_rangeFine = np.linspace(0.1, 20, 100000)
    F1Fine = np.interp(T_rangeFine, T_range, F1)
    F2Fine = np.interp(T_rangeFine, T_range, F2)

    def FindFrac1(Te):
        TheArray = np.zeros(len(Te))
        for i in range(len(Te)):
            differences = np.abs(T_rangeFine-Te[i])
            closest_index = np.argmin(differences)
            TheArray[i] = F1Fine[closest_index]

        return TheArray

    def FindFrac2(Te):
        TheArray = np.zeros(len(Te))
        for j in range(len(Te)):
            differences = np.abs(T_rangeFine - Te[j])
            closest_index = np.argmin(differences)
            TheArray[j] = F2Fine[closest_index]
        return TheArray

    #Note that FindFrac1CRM is to find the emission weighted fractional abundance. I made a different function for the EMC3 sim because inputting hundreds of thousands of points into calc_fz takes an extremely long time
    
    def FindFrac1CRM(Te):
        MakeArray = np.linspace(Te, Te, 1)
        te, fz, lz, lzl = ibal.calc_fz(elem='n', ne=(Density), te=(MakeArray), pn='~/ADAS/adf11')
        return fz[0][1]

    def FindPECexc(ne, Te):
        result = mydat.interpolate(ne, Te, 14)
        return result
    def FindPECrec(ne, Te):
        result = mydat.interpolate(ne, Te, 64)
        return result

    print("These are ni.values", ni.values)

    emissCRMVolData = VolData(grid=Te.grid, values = ni.values*1e12*n_totalnitrogen*FindFrac1(Te.values)*FindPECexc(ni.values*1e6, Te.values) + ni.values*1e12*n_totalnitrogen*FindFrac2(Te.values)*FindPECrec(ni.values*1e6, Te.values), units='J/s/m^2', symbol='Emissivity', rank='scalar')
                    
    plt.figure()
    emissCRMVolData.plot('rzslice', 10)
    plt.show()
    
    spec_system='AEI30_OP12b'
    coords, p0 = get_coords(spec_system)
    sh=np.shape(coords)

    spec_res=np.zeros((sh[0],))
 
    mask=np.zeros(np.shape(spec_res))
    
    
    
    
    
    Emissionlist = []
    EmissionlistCRM = []
   
    
    for i in range(sh[0]-10):
 
        
        #get LoS vector from spectrometer coordinates
        
        d=coords[i,:] - p0
        
        mag=0.
        for j in range(len(d)):
            mag+=d[j]*d[j]
        mag=np.sqrt(mag)
        v=d/mag
        
        
        #---------------------------------------------------------------
        #map the line of sight geometry
        #mapped[0] = LoS object
        #            LoS.gc - grouped cell centers = len(imap)
        #            LoS.gt - grouped cell vertices = len(imap)+1
        #mapped[1] = imap (eelab indices of VolData values along LoS)
        #            VolDataObj.get_values(imap) gets you the data
        #---------------------------------------------------------------
        
        if True:
            mapped=s.geometry.map_line_of_sight(p0, v, groupby_idcell=True)
            
            #---------------------------------------------------------------
            #Get differential distance along LoS for line integration
            #|----|----|----|----|----|----|
            #  d0   d1   d2   d3   d4   d5  
            #---------------------------------------------------------------
           
            x=mapped[0].x1
          
            y=mapped[0].x2

            z=mapped[0].x3
         
            r=np.sqrt(np.multiply(x,x) + np.multiply(y,y))
            phi = np.arctan2(y,x)*180/np.pi
            phi = np.abs(phi-2*72)
            Te_data = np.zeros((len(mapped[0].s),))
    
   
            
            
            d=np.zeros((len(mapped[0].s[1:-1])-2,))  
           
            for j in range(len(mapped[0].s[1:-1])-2):
                d[j]=mapped[0].s[j+2]-mapped[0].s[j+1]
            
            mapped_new = mapped[0].s[1:-1]

            #---------------------------------------------------------------
            #Get emission along the LoS (from Aurora)
            #for Nitrogen, the indices are:
            #14 - excitation of N1
            #64 - recombination of N2
            #---------------------------------------------------------------
            
           
            Te1d=np.array(Te.get_values(mapped[1])); ni1d=np.array(ni.get_values(mapped[1]))
            niN11d=np.array(niN1.get_values(mapped[1])); niN21d=np.array(niN2.get_values(mapped[1]))
            niN31d = np.array(niN3.get_values(mapped[1]));niN41d=np.array(niN4.get_values(mapped[1]))
            niN51d = np.array(niN5.get_values(mapped[1]));niN61d=np.array(niN6.get_values(mapped[1]))
            niN71d=np.array(niN7.get_values(mapped[1]))
            fN11d = niN11d/(niN11d+niN21d+niN31d+niN41d+niN51d+niN61d+niN71d)
            
            Emiss1d=np.array(emissVolData.get_values(mapped[1]))
            Emiss1dCRM = np.array(emissCRMVolData.get_values(mapped[1]))

            
            # Debugging for AEF line of sight:
            
        
            rV2 = r[3:-1]
            if len(rV2) == len(Emiss1d):
                plt.plot(rV2, Emiss1d)
            else:
                rV2 = r[2:-1]
                if len(rV2) == len(Emiss1d):
                    plt.plot(rV2, Emiss1d)
                else:
                    rV2 = r[1:-1]
                    if len(rV2) == len(Emiss1d):
                        plt.plot(rV2, Emiss1d)
      
            plt.title("Emissivity")
            plt.xlabel("Major radius (cm)")
            plt.ylabel("Emissivity ($Jm^{-2}s^{-1}$)")
            



            #excitation emission
            emiss1d = np.array(get_emission(fn, 1, Te1d, ni1d*1e6, niN11d*1e6) + get_emission(fn, 51, Te1d, ni1d*1e6, niN11d*1e6))
            emiss1dCRM = np.array(ni1d*1e12*(ni1d*0.02)*FindFrac1(Te1d)*FindPECexc(ni1d*1e6, Te1d) + ni1d*1e12*(ni1d*0.02)*FindFrac2(Te1d)*FindPECrec(ni1d*1e6, Te1d))
            
            Totion1d = (niN11d*1e6)/fN11d
            Totion1dCRM = (niN11d*1e6)/FindFrac1(Te1d)
            

            #I am going to write out the line integral method here
            Top_integral_result = 0
            Bottom_integral_result = 0
            Top_integral_result_nN1 = 0
            Bottom_integral_result_nN1 = 0
            Top_integral_resultf = 0
            Bottom_integral_resultf = 0
            
            Top_integral_resulttot = 0
            Bottom_integral_resulttot = 0
            Top_integral_resultdens = 0
            Bottom_integral_resultdens = 0
            
           
            Emissionlist = []
            Temperaturelist = []
            #Top integral:
            for i in range(len(d)):
                Emissionlist.append(emiss1d[i])
                Temperaturelist.append(Te1d[i])
                
            sorted_pairs = sorted(zip(Temperaturelist, Emissionlist))

            x_sorted, y_sorted = zip(*sorted_pairs)

            x_sorted = list(x_sorted)
            y_sorted = list(y_sorted)
            plt.figure()
            plt.plot(x_sorted, y_sorted)
            plt.xlim(0,30)
            plt.xlabel('Temperature (eV)')
            plt.ylabel('Emission')
            plt.title('Temperature binned emission along a line of sight')
            plt.show()

            
            
#Categories = np.linspace(1,15,15)
#You have to tinker between 15 and 16 for the lines of sight depending on the simulation
Categories = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16]
print("Length of Emissionlist", len(Emissionlist))
print("Emissionlist itself", Emissionlist)
print("Length of EmissionlistCRM", len(EmissionlistCRM))
print("EmissionlistCRM itself", EmissionlistCRM)

plt.figure()
plt.xlabel("Line of sight")
plt.ylabel("Line-integrated emission")
plt.bar(Categories, Emissionlist)
plt.title("AEI N3E13D.5_FRAD.2 EMC3")
plt.show()
plt.figure()
plt.xlabel("Line of sight")
plt.ylabel("Line-integrated emission")
plt.bar(Categories, EmissionlistCRM)
plt.title("AEI N3E13D.5_FRAD.2 CRM")
plt.show()




