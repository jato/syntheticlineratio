import numpy as np
import matplotlib.pyplot as plt
from eelab import Simulation
import atomdat.adas.adf15 as adf15
import atomdat.adas.adas as adas
import atomdat.adas.ibal as ibal
from eelab.dtypes import VolData
import matplotlib.cm as cm
#import aurora
from mpl_toolkits.axes_grid1.axes_divider import make_axes_locatable
from scipy.interpolate import splev
from scipy.interpolate import UnivariateSpline
import pandas as pd


def get_emission(fn, line,Te,ne, nimp, lim=10,lim2=1E19):
    '''
    Get emission from ADF15 file
    Inputs:
        fn   = name of the adf15 file
        line = idx of line (14 for 3996.13 exc, 114 for 3996.13 rec)
        Te   = array of Te values
        ne   = array of ne values
        nimp = array of impurity density values
        lim  = optional - if lim < Te value, then set emiss to 0
    '''
    mydat=adf15.ADF15()
    mydat.read(fn, v=1)
    mydat.print_line_index([3996,405])
    coeff=mydat.interpolate(ne,Te,line)
    
    emiss=np.multiply(coeff,np.multiply(ne,nimp))
    ind=np.where(Te > lim)
    emiss[ind]=0.
    ind2=np.where(ne <lim2)
    emiss[ind2]=0
    print("the coefficient is", coeff)
    
    return emiss



fn='pec98#n_ssh_pju#n1.dat'
mydat = adf15.ADF15()
mydat.read(fn, v=1)

#The line below represents the CRM model function
#te, fz, lz, lzl = ibal.calc_fz(elem='n', pn='~/ADAS/adf11/', fplt=True)

#ne_array2 = np.logspace(19, 21, 100)
ne_array2 = np.linspace(1e19, 1e21, 100)

#levels = np.linspace(6.5, 11, 10)

if __name__=='__main__':
   
    s=Simulation('/u/jato/EIM-m286A-Icc500A/P5MW/N3E13D.5_FRAD.8')
    
    # load needed parameters 
    Te=s['Te']; ni=s['ni']; niN1=s['niN1']; niN2=s['niN2']; Ti=s['Ti']; niN3=s['niN3']; niN4=s['niN4']; niN5=s['niN5']; niN6=s['niN6']; niN7=s['niN7']

    abundancelist1 = []
    abundancelist2 = []
    abundancelist3 = []
    abundancelist4 = []
    abundancelist5 = []
    abundancelist6 = []
    abundancelist7 = []
    abundancesum = []
    Telist1 = []
    densitylist = []
    dlist = []

#I am going to modify the generation of my grid slightly to include the divertor region rather than the cross section
#I am to generate lists of fractional abundances and density and temperatures for each point in the divertor region. I will then use this database to inform the generation of the contour plot.

    R_range = np.linspace(500,580, 60)
    Z_range = np.linspace(85, 100, 60)
    phi_range = np.linspace(0, 19, 60)

    for k in phi_range:
        for j in Z_range:
            for i in R_range:
                try:
                    if Te(i,j,k) <= 20:
                       
                        niN1_bin = np.zeros(1)
                        niN2_bin = np.zeros(1)
                        niN3_bin = np.zeros(1)
                        niN4_bin = np.zeros(1)
                        niN5_bin = np.zeros(1)
                        niN6_bin = np.zeros(1)
                        niN7_bin = np.zeros(1)
                        niNtot_bin = np.zeros(1)
                        
                        d_bin = np.zeros(1)
                        
                        
                        Te_Bin = np.zeros(1)
                        niN1_bin[0] = niN1(i,j,k)
                        niN2_bin[0] = niN2(i,j,k)
                        niN3_bin[0] = niN3(i,j,k)
                        niN4_bin[0] = niN4(i, j, k)
                        niN5_bin[0] = niN5(i, j, k)
                        niN6_bin[0] = niN6(i, j, k)
                        niN7_bin[0] = niN7(i, j, k)
                        niNtot_bin[0] = niN1(i, j, k) + niN2(i, j, k) + niN3(i, j, k) +niN4(i, j, k) + niN5(i, j, k) +niN6(i, j, k) + niN7(i, j, k)
                        d_bin[0] = ni(i,j,k)*1e6
                        dlist.append(ni(i,j,k)*1e6)
                        Telist1.append(Te(i,j,k))
                        Te_Bin[0] = Te(i, j, k)

                        

                        
                        abundancelist1.append(niN1_bin/niNtot_bin)
                        abundancelist2.append(niN2_bin/niNtot_bin)
                        abundancelist3.append(niN3_bin/niNtot_bin)
                        abundancelist4.append(niN4_bin/niNtot_bin)
                        abundancelist5.append(niN5_bin/niNtot_bin)
                        abundancelist6.append(niN6_bin/niNtot_bin)
                        abundancelist7.append(niN7_bin/niNtot_bin)
                        
                        
                        
                    else:
                        continue
                except:
                    continue
    #Tallying the entire divertor region produces a large scatter, especially since we are dealing with tens of thousands of points. I therefore take chunks of data points and take the average.

    def chunked_average(data, chunk_size):
        reshaped = data[:len(data)-len(data) % chunk_size].reshape(-1, chunk_size)
        mean = reshaped.mean(axis=1)
        se = reshaped.std(axis=1) / np.sqrt(chunk_size)
        return mean, se
    
    abundancelist1 = np.array(abundancelist1)
    abundancelist2 = np.array(abundancelist2)
    abundancelist3 = np.array(abundancelist3)
    abundancelist4 = np.array(abundancelist4)
    abundancelist5 = np.array(abundancelist5)
    abundancelist6 = np.array(abundancelist6)
    abundancelist7 = np.array(abundancelist7)
    Telist1 = np.array(Telist1)
    chunk_size = 400
    x_reduced = chunked_average(Telist1, chunk_size)[0]
    y_reduced, y_se = chunked_average(abundancelist1, chunk_size)
    y_reduced2, y_se2 = chunked_average(abundancelist2, chunk_size)
    y_reduced3, y_se3 = chunked_average(abundancelist3, chunk_size)
    y_reduced4, y_se4 = chunked_average(abundancelist4, chunk_size)
    y_reduced5, y_se5 = chunked_average(abundancelist5, chunk_size)
    y_reduced6, y_se6 = chunked_average(abundancelist6, chunk_size)
    y_reduced7, y_se7 = chunked_average(abundancelist7, chunk_size)
    


    sorted_indices = np.argsort(x_reduced)
    x_reduced = x_reduced[sorted_indices]
    y_reduced = y_reduced[sorted_indices]
    y_reduced2 = y_reduced2[sorted_indices]
    y_reduced3 = y_reduced3[sorted_indices]
    y_reduced4 = y_reduced4[sorted_indices]
    y_reduced5 = y_reduced5[sorted_indices]
    y_reduced6 = y_reduced6[sorted_indices]
    y_reduced7 = y_reduced7[sorted_indices]
    
    spline1 = UnivariateSpline(x_reduced, y_reduced, s=0.5)
    spline2 = UnivariateSpline(x_reduced, y_reduced2, s=0.5)
    spline3 = UnivariateSpline(x_reduced, y_reduced3, s=0.5)
    spline4 = UnivariateSpline(x_reduced, y_reduced4, s=0.5)
    spline5 = UnivariateSpline(x_reduced, y_reduced5, s=0.5)
    spline6 = UnivariateSpline(x_reduced, y_reduced6, s=0.5)
    spline7 = UnivariateSpline(x_reduced, y_reduced7, s=0.5)

    x_dense = np.linspace(1,10,100)
    y_dense1 = spline1(x_dense)
    y_dense2 = spline2(x_dense)
    y_dense3 = spline3(x_dense)
    y_dense4 = spline4(x_dense)
    y_dense5 = spline5(x_dense)
    y_dense6 = spline6(x_dense)
    y_dense7 = spline7(x_dense)

    plt.figure()
    plt.plot(x_dense, y_dense1)
    plt.plot(x_dense, y_dense2)
    plt.plot(x_dense, y_dense3)
    plt.plot(x_dense, y_dense4)
    plt.plot(x_dense, y_dense5)
    plt.plot(x_dense, y_dense6)
    plt.plot(x_dense, y_dense7)
    plt.show()
    
    

    BigGrid = np.zeros((100,100))
    fzgrid = np.zeros((100, 100))

    



    df = pd.read_csv('Impurity_relative_charge_state_density_near_SOL.txt', delimiter='\t', header=0, encoding='utf-8')
    df.columns = df.columns.str.strip()
    df = df.apply(lambda x: x.str.stripy() if x.dtype == "object" else x)

    #in the below it was a bit tricky to get the right column, i couldn't seem to call it by name so I manually called the index which appropriately matched
    electron_temperature = df[df.columns[0]].values
    N_dens_0 = df[df.columns[1]].values
    N_dens_1 = df[df.columns[2]].values
    N_dens_2 = df[df.columns[3]].values
    N_dens_3 = df[df.columns[4]].values
    N_dens_4 = df[df.columns[5]].values
    N_dens_5 = df[df.columns[6]].values
    N_dens_6 = df[df.columns[7]].values
    N_dens_7 = df[df.columns[8]].values

    Sabundancelist1 = []
    Sabundancelist2 = []
    Sabundancelist3 = []
    Sabundancelist4 = []
    Sabundancelist5 = []
    Sabundancelist6 = []
    Sabundancelist7 = []
    Templist = []

    for i in range(len(electron_temperature)):
        #The i >=57 here represents the cordoning off of Sergei's data to match his abundance plot
        #Once I get the fractional abundance plot I just repeat what I did with the EMC3 abundance plot to obtain the contour plot (coded towards bottom)
        if i >= 57:
            tot_ion_density = N_dens_1[i]+N_dens_2[i]+N_dens_3[i]+N_dens_4[i]+N_dens_5[i]+N_dens_6[i]+N_dens_7[i]
            fA1 = N_dens_1[i]/tot_ion_density
            Sabundancelist1.append(fA1)
            fA2 = N_dens_2[i]/tot_ion_density
            Sabundancelist2.append(fA2)
            fA3 = N_dens_3[i]/tot_ion_density
            Sabundancelist3.append(fA3)
            fA4 = N_dens_4[i]/tot_ion_density
            Sabundancelist4.append(fA4)
            fA5 = N_dens_5[i]/tot_ion_density
            Sabundancelist5.append(fA5)
            fA6 = N_dens_6[i]/tot_ion_density
            Sabundancelist6.append(fA6)
            fA7 = N_dens_7[i]/tot_ion_density
            Sabundancelist7.append(fA7)
            Templist.append(electron_temperature[i])
    
    sorted_indices = np.argsort(Templist)

    Templist = np.array(Templist)
    Sabundancelist1 = np.array(Sabundancelist1)
    Sabundancelist2 = np.array(Sabundancelist2)
    Sabundancelist3 = np.array(Sabundancelist3)
    Sabundancelist4 = np.array(Sabundancelist4)
    Sabundancelist5 = np.array(Sabundancelist5)
    Sabundancelist6 = np.array(Sabundancelist6)
    Sabundancelist7 = np.array(Sabundancelist7)



    
    Templist = Templist[sorted_indices]
    Sabundancelist1 = Sabundancelist1[sorted_indices]
    Sabundancelist2 = Sabundancelist2[sorted_indices]
    Sabundancelist3 = Sabundancelist3[sorted_indices]
    Sabundancelist4 = Sabundancelist4[sorted_indices]
    Sabundancelist5 = Sabundancelist5[sorted_indices]
    Sabundancelist6 = Sabundancelist6[sorted_indices]
    Sabundancelist7 = Sabundancelist7[sorted_indices]



    x_denses = np.linspace(1,10,100)
    y_dense1s = np.interp(x_denses, Templist, Sabundancelist1)
    y_dense2s = np.interp(x_denses, Templist, Sabundancelist2)
    y_dense3s = np.interp(x_denses, Templist, Sabundancelist3)
    y_dense4s = np.interp(x_denses, Templist, Sabundancelist4)
    y_dense5s = np.interp(x_denses, Templist, Sabundancelist5)
    y_dense6s = np.interp(x_denses, Templist, Sabundancelist6)
    y_dense7s = np.interp(x_denses, Templist, Sabundancelist7)


    
   
    PECrecs = []
    CRMlistf1 = []
    CRMlistf2 = []
    AGrid = np.zeros((100,100))
    x_dense2 = np.linspace(1,10,100)
    levels = np.linspace(0.01, 0.05, 5)
    for i in range(len(x_dense)):
        MakeArray2 = np.linspace(x_dense[i], x_dense[i], 1)
        te, fz, lz, lzl = ibal.calc_fz(elem='n', ne=(2e20), te = (MakeArray2), pn = '~/ADAS/adf11')
        MakeArray = np.linspace(1e20, 1e20, 1)
        result = mydat.interpolate(MakeArray, MakeArray2, 20)
        PECrecs.append(result)
        CRMlistf1.append(fz[0][1])
        CRMlistf2.append(fz[0][2])
    

    plt.figure()
    plt.plot(x_dense, y_dense1s, linestyle=':', color='blue')
    plt.plot(x_dense, y_dense2s, linestyle=':', color='orange')
    plt.plot(x_dense, y_dense1, linestyle='--', color='blue')
    plt.plot(x_dense, y_dense2, linestyle='--', color='orange')
    plt.plot(x_dense, CRMlistf1, linestyle='-', color='blue')
    plt.plot(x_dense, CRMlistf2, linestyle='-', color='orange')
    
    plt.show()


    for i in range(len(x_dense)):
        MakeArray2 = np.linspace(x_dense[i], x_dense[i], 1)
        for j in range(len(ne_array2)):
            MakeArray = np.linspace(ne_array2[j], ne_array2[j], 1)
            result = mydat.interpolate(MakeArray, MakeArray2, 14)
            result2 = mydat.interpolate(MakeArray, MakeArray2, 18)
            resultrec = mydat.interpolate(MakeArray, MakeArray2, 64)
            result2rec = mydat.interpolate(MakeArray, MakeArray2, 68)

          #  BigResult = (y_dense1[i]*result2+y_dense2[i]*result2rec)/(y_dense1[i]*result+y_dense2[i]*resultrec)
            BigGrid[j][i] = (y_dense1[i]*result2+y_dense2[i]*result2rec)/(y_dense1[i]*result+y_dense2[i]*resultrec)

    for i in range(len(x_dense)):
        MakeArray2 = np.linspace(x_dense[i], x_dense[i], 1)
        for j in range(len(ne_array2)):
            MakeArray = np.linspace(ne_array2[j], ne_array2[j], 1)
            result = mydat.interpolate(MakeArray, MakeArray2, 14)
            result2 = mydat.interpolate(MakeArray, MakeArray2, 18)
            resultrec = mydat.interpolate(MakeArray, MakeArray2, 64)
            result2rec = mydat.interpolate(MakeArray, MakeArray2, 68)

           # BigResult = (y_dense1s[i]*result2+y_dense2s[i]*result2rec)/(y_dense1s[i]*result+y_dense2s[i]*resultrec)
            AGrid[j][i] = (y_dense1s[i]*result2+y_dense2s[i]*result2rec)/(y_dense1s[i]*result+y_dense2s[i]*resultrec)
    
    for i in range(len(x_dense)):
        MakeArray2 = np.linspace(x_dense[i], x_dense[i], 1)
        for j in range(len(ne_array2)):
            MakeArray = np.linspace(ne_array2[j], ne_array2[j], 1)
            result = mydat.interpolate(MakeArray, MakeArray2, 14)
            result2 = mydat.interpolate(MakeArray, MakeArray2, 18)
            resultrec = mydat.interpolate(MakeArray, MakeArray2, 64)
            result2rec = mydat.interpolate(MakeArray, MakeArray2, 68)

            te, fz, lz, lzl = ibal.calc_fz(elem = 'n', ne=(MakeArray), te = (MakeArray2), pn = '~/ADAS/adf11')
           # BigResult = (fz[0][1]*result2+ fz[0][2]*result2rec)/(fz[0][1]*result+fz[0][2]*resultrec)
            fzgrid[j][i] = (fz[0][1]*result2+ fz[0][2]*result2rec)/(fz[0][1]*result+fz[0][2]*resultrec)

    CRMlistf1 = []
    CRMlistf2 = []
    PECrecs = []
    x_dense2 = np.linspace(1,21,100)

    anotherlistlol1 = []
    anotherlistlol2 = []
    anotherlistlol3 = []
    PEClist = []
    PECexclist = []
    PECreclist = []
    excitation1 = []
    excitation2 = []
    excitation3 = []
    recomb1 = []
    recomb2 = []
    recomb3 = []
   # levels = np.linspace(0.1, 1.2, 6)
    x_dense3 = np.linspace(1,3,100)
    for i in range(len(x_dense)):
        MakeArray2 = np.linspace(x_dense[i], x_dense[i], 1)
        MakeArray = np.linspace(3e19,3e19, 1)
        result = mydat.interpolate(MakeArray, MakeArray2, 18)
        resultrec = mydat.interpolate(MakeArray, MakeArray2, 68)
        result2 = mydat.interpolate(MakeArray, MakeArray2, 14)
        result2rec = mydat.interpolate(MakeArray, MakeArray2, 64)

        te, fz, lz, lzl = ibal.calc_fz(elem = 'n', ne=(MakeArray), te = (MakeArray2), pn = '~/ADAS/adf11')
        anotherlistlol1.append((fz[0][1]*result + fz[0][2]*resultrec)/(fz[0][1]*result2+fz[0][2]*result2rec))
        anotherlistlol2.append((y_dense1[i]*result + y_dense2[i]*resultrec)/(y_dense1[i]*result2 + y_dense2[i]*result2rec))
        anotherlistlol3.append((y_dense1s[i]*result + y_dense2s[i]*resultrec)/(y_dense1s[i]*result2 + y_dense2s[i]*result2rec))
        PEClist.append((result+resultrec)/(result2+result2rec))
        PECexclist.append(result/result2)
        PECreclist.append(resultrec/result2rec)
        print("result", result)
        print("result2", result2)
        print("resultrec", resultrec)
        print("result2rec", result2rec)
      

        
    plt.figure()
    plt.plot(x_dense, anotherlistlol1, linestyle='-', color='blue',  label='CRM')
    plt.plot(x_dense, anotherlistlol2, linestyle='-', color='orange',  label='EMC3')
    plt.plot(x_dense, anotherlistlol3, linestyle='-', color='green',  label='AUG')
    plt.plot(x_dense, PEClist, linestyle = '-', color='black', label='PECs alone')
    plt.plot(x_dense, PECexclist, linestyle='-', color='red', label='PECs exc alone')
    plt.plot(x_dense, PECreclist, linestyle='-', color='purple', label='PECs rec alone')
   # plt.plot(x_dense, excitation1, linestyle='--', color='blue')
   # plt.plot(x_dense, excitation2, linestyle='--', color='orange')
   # plt.plot(x_dense, excitation3, linestyle='--', color='green')
   # plt.plot(x_dense, recomb1, linestyle=':', color='blue')
   # plt.plot(x_dense, recomb2, linestyle=':', color='orange')
   # plt.plot(x_dense, recomb3, linestyle=':', color='green')

    
    plt.legend()
    plt.show()

    


    print(PECrecs)
    
   # print(CRMlistf2)
    #This part plots the two fractional abundances N1+ and N2+ for the three different methods



    plt.figure()
    blue_patch = plt.Line2D([0], [0], color='blue', label='EMC3-EIRENE derived')
    black_patch = plt.Line2D([0], [0], color='black', label='CRM model derived')
    green_patch = plt.Line2D([0], [0], color='green', label='AUG derived')
    plt.legend(handles=[blue_patch, black_patch, green_patch])
    contour1 = plt.contour(x_dense, ne_array2, BigGrid, colors='blue', levels=levels)
    contour2 = plt.contour(x_dense, ne_array2, fzgrid, colors='black', levels=levels)
    contour3 = plt.contour(x_dense, ne_array2, AGrid, colors='green', levels=levels)

    plt.clabel(contour1, inline=True, fontsize=8)
    plt.clabel(contour2, inline=True, fontsize=8)
    plt.clabel(contour3, inline=True, fontsize=8)
    plt.yscale('log')
    plt.title('402.6/399.5 contour comparison')
    plt.show()



     


